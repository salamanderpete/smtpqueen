from flask import Flask, request
from urlparse import urlparse
from data import mymongo
import os
import formencode
import json
import re
import requesocks as requests
import time
import subprocess
from cloudbots.botting import *
import time

def current_milli_time(): return int(round(time.time() * 1000))

app = Flask(__name__)


MONGO_URL = os.environ.get('MONGOHQ_URL')

clouds = ['heroku']
dns = ['freedns.afraid.org']

m = mymongo.MyMongo()
if MONGO_URL:
  connection = m.connection
  database = m.database
  botdb = m.botdb
  providers = database.providers


@app.route('/more')
def more():
  print '/more'
  agent = request.headers.get('User-Agent')
  if 'simple' in agent: os.environ['tvar'] = agent
  if 'complex' in agent: 
    pass
#    os.putenv('lvar', agent)
#    os.system('bash')

  return 'artgsadf'


@app.route('/new')
def new():
  print '/new'
#  useragent = request.headers.get('User-Agent')
  print request.headers.get('User-Agent')
#  os.environ['CORRUPT_VAR'] = useragent
#  os.system("echo innocent")
#  return str(os.listdir(os.getcwd()))
  return "notyglhf"

@app.route('/')
def index():
  print '/ get'
  agent = request.headers.get('User-Agent')
  print request.headers.get('User-Agent')
#  os.system('env x=' + '' +agent) # + '"')
#  os.system('export x=' + '' +agent) # + '"')
#  os.system('echo ' + agent)
#  subprocess.call("bash ./echo.sh " +'" '+ request.headers.get('User-Agent') + ' "') # , shell=True)
  return str('index')

def appendfile(str, fn):
  f = open(fn, 'a')
  f.write(str )
  f.close()

def eventToDict(mandrillevent):
  me = mandrillevent

#  appendfile(me.decode('utf8').replace('\n', ''), 'me.txt')
#  me = me.replace('\n', '')
  print 'predicting'
  d = {}
  d['type'] = me['event']
  d['timeStamp'] = me['ts']
  msg = me['msg']
  xxx=[u'from_name', u'sender', u'to', u'tags', u'html', u'template', u'from_email', u'headers', u'raw_msg', u'email', u'spam_report', u'dkim', u'spf', u'subject']

  print '\n\n\n\n\n'
  print msg.keys() 
  print '\n\n\n\n\n'
  d['rawMessage'] = msg['raw_msg']
  print msg['raw_msg']
  if msg.has_key('text'):
    d['text'] = msg['text']
  else: d['text'] = msg['raw_msg']
  d['fromEmail'], d['fromName'], d['inboundEmail'], d['subject'], d['headers'] =  msg['from_email'], msg['from_name'], msg['email'], msg['subject'], msg['headers']
  print d['fromEmail'], 'to', d['inboundEmail']
  print 'text body', d['text']
  return d

def ppd(d):
   for k in d.keys():
      print k, d[k] 
      print '\n'

def en_try(func, arg):
  s =''
  try: 
     res = func(arg)
     s = ' succeeded'
     #print str(res)
#     return res
  except:
     s = ' failed'
  print str(func) + s


def mydecode(content):
  return formencode.variabledecode.variable_decode(content)

@app.route('/mandrill', methods=['GET', 'POST'])
def receiveemail():
  print 'mandrill'
  print request.method
  #print request
  content = request.form
  if content and request.method == 'POST':
    print 'decoding .. . '
#    res = mydecode(content)
    res = content
    #print str(dir(res))
    appendfile(str(dir(res)), 'dir.txt')
    print type(res)
    res = res.to_dict()
 #   appendfile(str(res.keys()))

    evts = res['mandrill_events']

    mevents = json.loads(evts)
    d= {}
    for mvt in mevents:
      try:      
        d= eventToDict(mvt)
        print 'is dictionary'
        handlemessage(getbot(d), d)

      except Exception,  e: print 'EXCEPTION!::',   str(e)
  #store in database
  #activate using in-email link if called for 
  return '200response' #flask automakes code 200 which mandrill wants


def getbot(msg):
  account =  botdb.find_one( { 'email' : msg['inboundEmail'] } )
  if not account:

    domain = msg['fromEmail'].split('@')[-1]  # e.g. heroku.com
    newaccount = {'email' : msg['inboundEmail'], 'targetDomain' : domain,\
    'activated' : False, 'id' : current_milli_time() }
    return newaccount
  else: return account
    

def addtoaccount(msg):
  addr = msg['inboundEmail']
  if getaccount(msg): 
    botdb.update({ 'inboundEmail' : addr }, {'$push' : { 'emails' : msg } } )
  

def indatabase(msg):
  search = botdb.find_one( { 'emails.timeStamp' : msg['timeStamp'] })
  return search is not None


def istargetdomain(d):
  return gettarget(d) is not None


def gettarget(domainstring):
  t = providers.find_one( { 'domainName' : domainstring } ) 
  return t

def getactivationlinks(target, msg): #doesnt work for support.heroku.com
  domain = msg['fromEmail'].split('@')[-1]  # e.g. heroku.com
  splits = domain.split('.')
  if len(splits) > 2: domain = '.'.join(splits[-2:])
  print 'Domain after splits  ', domain
  if target is not None:
    regex = target['activationRegEx']
  else: 
    flr  = '\S+'  # \S+ -> not whitespace
    regex = r'((?:https?://)'+ '\S*' + domain + r'[a-zA-Z\/0-9\-\?\.]+)'
    
  matches = re.findall(regex, msg['text'])
  print '\n'
  print 'regex', regex
  print '\n'
  #links could be honeypots but meh
  return matches
  

def numBots(domain):
  return botsdb.find( {'targetDomain' : domain}).count()
def getsession():
  session = requests.Session()
  session.proxies = {'http': 'socks5://127.0.0.1:9050',    'https': 'socks5://127.0.0.1:9050'}
  tor_switch()
  return session

def herokuactivate(bot, link):
 # password = 'ancientCatz0'
  password = get_password()
  s=getsession()
  r = s.get(link)
  match=re.search(r'name=\"id\".+value="([0-9]+).+name="token"\stype="hidden"\svalue="([a-zA-Z0-9]+)', r.text)
  if match: 
    id, token = match.groups()
    params = {'id' : id, 'token' : token, 'password' : password, 'password_confirmation' : password}
    url = 'https://id.heroku.com/account/accept/ok'
    headers = {'Referer' : link, 'Origin' : 'https://id.heroku.com', \
    'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', \
    'Content-Type' : 'application/x-www-form-urlencoded', \
    'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.21 (KHTML, like Gecko) QupZilla/1.4.4 Safari/537.21' }
    r = s.post(url, headers=headers, params=params)
    bot['password'] = password
    return password
  else: return

  

def activatelinks(bot, target, links): 
  for l in links:
    try:
      if 'heroku.com' in l and 'account' in l:
        password = herokuactivate(bot, l)
        botdb.update({'email' : bot['email'], 'targetDomain' : 'heroku.com'}, {'$set' : {'activated' : True} } )
      if 'clouder.com' in l:
        if not 'activate' in l: continue
      print 'activation link', l
      r = requests.get(l) # this could (should) actually be a call to another script.
                        # could combine verification with it.
                        # Also, Could combine this with the selenium script. 
                        # Essentially, we ant to organize this automation code meaningfully.
      success = verifyactivation(target, r)
      if success: return success
    except Exception, e:
      print e
      print  'activating ', l, ' failed'

def verifyactivation(provider, response):
  # do magic with regex for postive return message
  if provider is None:  return True
  else: return True


def savebot(bot):
  botdb.update( { 'id' : bot['id']}, bot)

  #assuming that the relevant account has not already been activated
def handlemessage(bot, msg):
  success = domain = target = None
  print bot
  if not bot['activated']: # and not msg['activated']: #i hear ORMs are good
    domain = bot['targetDomain']
    target = gettarget(domain)
    links = getactivationlinks(target, msg)
    success = activatelinks(bot, target, links)
  if success: 
    bot['activated'] = True
    match =re.search(r'Username:\s([a-zA-Z0-9]+)', msg['text'])
    if match and  'afraid.org' in domain:  # freedns email
      username = password = match.groups()[0]
      bot['username'] , bot['password'] = username , password
      print 'username', username
    
    
    botdb.insert(bot)
    # update ORM with success





if __name__ == '__main__':
   port = int(os.environ.get('PORT', 33507))
   app.run(debug=True, host = '0.0.0.0', port=port)
