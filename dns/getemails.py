import re
import mymongo
import pymongo

regex = r"([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*)@((?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"

db = mymongo.MyMongo()
sourcefile = open('emails.txt', 'r')
outfile = open('usernames.txt', 'a')
source = sourcefile.read()
print db.botdb
matches = re.findall( regex, source)
emails = []
usernames = []
db.emails.ensure_index([("username", pymongo.ASCENDING),  ("unique" , True),  ("dropDups" , True)])


for tuple in matches:
  user, domain = tuple
  usernames.append(user)
  emails.append({'username' : user, 'domain' : domain})

outstring = '\n'.join(usernames)
outfile.write(outstring)

#db.emails.insert(emails)

print str(db.emails.count()) + '  emails loaded'

outfile.close()
sourcefile.close()
db.close()
