
import os
import time
import requests
import re
from datetime import datetime
import mymongo
import pymongo
'''
stillpublic: because we're sorting by status (public|private) we will eventually hit a page where everything past it is
private, so we stop there.
pagenum: iterate through each page
sort: sort by status
regex: matches groups in the form (id (url), domain name, # hosts, status, datecreated)
'''


# Start real code
# matches privates: domainre =  r'class="tr.".+edit_domain_id=([0-9]+)>([a-zA-Z0-9]+\.[a-z]+)<.+\(([0-9]+)\ h.+(public|private).+\(([0-9]{2}\/[0-9]{2}\/[0-9]{4})\)'


def storedomains(tuples):
  domainlist = []
  for domaintuple in tuples:
    id, domainname, hosts, status, createDate = domaintuple
    id, hosts = int(id), int(hosts)
    ispublic = (status == 'public')
    dt = datetime.strptime(createDate, '%m/%d/%Y')
    d = {'webid' : id, 'domainName' : domainname, 'hosts' : int(hosts), 'isPublic' : ispublic, 'createTime' : dt}
    domainlist.append(d)
#  if not mymongo.domains.find_one({'webid' : id}):
  mymongo.domains.insert(domainlist)













domainre =  r'class="tr.".+edit_domain_id=([0-9]+)>([a-zA-Z0-9]+\.[a-z]+)<.+\(([0-9]+)\ h.+(public).+\(([0-9]{2}\/[0-9]{2}\/[0-9]{4})\)'

url = 'http://freedns.afraid.org/domain/registry/'

mymongo = mymongo.MyMongo()
#mymongo.domains.ensure_index([("webid", pymongo.ASCENDING),  ("unique" , True),  ("dropDups" , True)])
pagenum = 0 #already started
#
#
# NOTE:  Last time our dns names stopped at page 321
#
#Stopped at page 321, ran out of public domains, only private domains past 321
stillpublic = True
while stillpublic:
  pagenum += 1

  #sort=2 sorts by status (public v. private)
  params = {'page' : pagenum, 'sort' : 2}
  r = requests.get(url, params=params)
  # example:  ('24459', 'ricardo24.ch', '56', 'public', '07/22/2004'),

  matches = re.findall(domainre, r.text)
  if not matches or len(matches) == 0:
    stillpublic = False
    print ' Ran out of public domains at page  ' + str(pagenum)
  else:
    storedomains(matches)
    print 'Stored ' + str(len(matches)) + ' public domains from page ' + str(pagenum)

  time.sleep(1) #be polite, no rush


