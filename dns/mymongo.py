import pymongo
import urlparse
import os

class MyMongo:
  def __init__(self):

    MONGO_URL = os.environ.get('MONGOHQ_URL')
    if MONGO_URL:
      self.connection = pymongo.Connection(MONGO_URL)
      database = self.connection[urlparse.urlparse(MONGO_URL).path[1:]]
      self.botdb = database.botdb
      self.providers = database.providers
      self.domains = database.domains
      self.emails = database.emails


  def close(self):
    self.connection.close()
