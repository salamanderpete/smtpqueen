<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>selenium scripts - Pastebin.com</title>
		<link rel="shortcut icon" href="/favicon.ico" />
<link href="/cache/css/text.css" rel="stylesheet" type="text/css" />		<link href="/i/main1.css" rel="stylesheet" type="text/css" />
				
		<script type="text/javascript" src="/js/jquery.js"></script>
		<script type="text/javascript" src="/js/main_v1.js"></script>
		<meta property="fb:app_id" content="231493360234820" />
		<meta property="og:title" content="selenium scripts - Pastebin.com" />
		<meta property="og:type" content="article" />
	    <meta property="og:url" content="http://pastebin.com/5XYTcDE0" />
	    <meta property="og:image" content="http://pastebin.com/i/fb2.jpg" />
	    <meta property="og:site_name" content="Pastebin" />
		<meta name="google-site-verification" content="jkUAIOE8owUXu8UXIhRLB9oHJsWBfOgJbZzncqHoF4A" />
		<link rel="canonical" href="http://pastebin.com/5XYTcDE0" />
				<!--[if SafMob]>
			<style>body{-webkit-text-size-adjust:none;}</style>
		<![endif]-->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-58643-34']);
		  _gaq.push(['_trackPageview']);
			setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])", 15000);
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	</head>
	<body>
	<div id="super_frame">
		<div id="logo" onclick="location.href='/'" title="Create New Paste"></div>
		<div id="header">
			<div id="header_top">
				<span class="span_left more">PASTEBIN</span><span class="span_left less"> &nbsp;|&nbsp; #1 paste tool since 2002</span><span class="min_max_span narrow_it" title="Change layout width"></span><span class="min_max_span wide_it" style="display:none" title="Change layout width"></span>				<ul class="top_menu">
					<li class="no_border_li"><a href="/" accesskey="n">create new paste</a></li><li><a href="/tools">tools</a></li><li><a href="/api">api</a></li><li><a href="/archive">archive</a></li><li><a href="/faq">faq</a></li>
				</ul>
			</div>
			<div id="header_middle">
				<span class="span_left big"><a href="/">PASTEBIN</a></span> 
				<a href="http://twitter.com/pastebin" target="_blank"><img src="/i/t.gif" alt="" class="i_tf" width="122" height="20" border="0" /></a>	<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpastebin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=segoe+ui&amp;height=21&amp;appId=231493360234820" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;margin:13px 0 0 10px;vertical-align:top" allowTransparency="true"></iframe>
				<div id="header_middle_search">
					<form class="search_form" name="search_form" method="get" action="/search" id="cse-search-box">
					    <input type="hidden" name="cx" value="013305635491195529773:0ufpuq-fpt0" />
					    <input type="hidden" name="cof" value="FORID:10" />
					    <input type="hidden" name="ie" value="UTF-8" />
						<input class="search_input" type="text" name="q" size="5" value="" placeholder="search..." x-webkit-speech/><input name="sa" class="search_button" src="/i/t.gif" alt="Search..." type="image" value="Search" />
					</form>
				</div>					
			</div>
			<div id="header_bottom">
				<div class="div_top_menu">
					<img src="/i/t.gif" class="i_n" width="16" height="16" alt="" border="0" /> <a href="/">create new paste</a> 
					&nbsp;&nbsp;&nbsp; <img src="/i/t.gif" class="i_t" width="16" height="16" alt="" border="0" /> <a href="/trends">trending pastes</a>
				</div>
				<ul class="top_menu">
					<li class="no_border_li"><a href="/signup">sign up</a></li><li><a href="/login">login</a></li><li><a href="/alerts">my alerts</a></li><li><a href="/settings">my settings</a></li><li><a href="/profile">my profile</a></li>				</ul>		
			</div>			
		</div>

			<div class="frame_spacer"><!-- --></div>
			<div style="height:35px;line-height:35px;font-size:0.85em;"><img src="/i/tip.png" width="16" height="16" style="vertical-align:middle;margin:-6px 0 0 2px" alt="" /> Pastebin launched a little side project called <a href="http://veryviral.com/" target="_blank">VERYVIRAL.com</a>, check it out ;-) <span style="float:right;text-align:right;">Want more features on Pastebin? <a href="/signup"><b>Sign Up</b></a>, it's FREE!</span></div>
		<div class="frame_spacer"><!-- --></div>
		<div id="monster_frame">
			<div id="content_frame">
				<div id="content_right">
															<div class="content_right_menu">
									<div class="content_right_title"><a href="/archive">Public Pastes</a></div>	
									<div id="menu_2">
										<ul class="right_menu"><li><a href="/1VaR0DeB">Untitled</a><span>8 sec ago</span></li><li><a href="/WzvbDuFC">Untitled</a><span>49 sec ago</span></li><li><a href="/LSgL2fEG">SQL Acl</a><span>SQL | 18 sec ago</span></li><li><a href="/3zrFkAAd">great first post! ...</a><span>31 sec ago</span></li><li><a href="/pwZMmN18">Minecraft Blaze</a><span>Java | 24 sec ago</span></li><li><a href="/bUb9BtUj">Fatty Art Pack</a><span>36 sec ago</span></li><li><a href="/bWgnZXNz">Jonathan Milner</a><span>53 sec ago</span></li><li><a href="/YvFRxErf">Untitled</a><span>1 min ago</span></li></ul></div></div>					
			<div style="padding: 0; width:160px;margin: 10px 0;clear:left;">
				<script type="text/javascript"><!--
					e9 = new Object();
				    e9.size = "160x600,120x600";
				//--></script>
				<script type="text/javascript" src="http://tags.expo9.exponential.com/tags/Pastebincom/Unsure/tags.js"></script>
			</div>					<div id="steadfast" title="Pastebin is proudly hosted by Steadfast.net" onclick="location.href='http://steadfast.net/?utm_source=pastebin.com&utm_medium=referral&utm_content=hosting_by_banner&utm_campaign=referral_20140118_x_x_pastebin_partner&source=referral_20140118_x_x_pastebin_partner'"></div>
				</div>
				<div id="content_left">

	<div class="paste_box_frame">
		<div class="tweet">


			<div onclick="facebookpopup('/5XYTcDE0'); return false;" id="b_facebook2"><span id="b_facebook"></span></div>
			<div onclick="twitpopup('/5XYTcDE0'); return false;" id="b_twitter2"><span id="b_twitter"></span></div>

		</div>
		<div class="paste_box_icon">
			<img src="/i/t.gif" class="i_gb" alt="Guest" border="0" />	
		</div>
		<div class="paste_box_info">
			<div class="paste_box_line1" title="selenium scripts"><img src="/i/t.gif"  class="i_p0" width="16" height="16" title="Public paste, everybody can see this paste." alt=""  border="0" /><h1>selenium scripts</h1> </div>
			<div class="paste_box_line2">By: <a href="/u/wovenhead">wovenhead</a>  on <span title="Tuesday 16th of September 2014 05:40:36 PM CDT" style="cursor:help;border-bottom: 1px dotted;">Sep 16th, 2014</span> &nbsp;|&nbsp; syntax: <a href="/archive/text">None</a> &nbsp;|&nbsp; size: 1.57 KB &nbsp;|&nbsp; views: <span title="Number of unique visitors to this paste" style="cursor:help;border-bottom: 1px dotted;">100</span> &nbsp;|&nbsp; expires: Never</div>
			<div class="paste_box_line3"><a href="/download.php?i=5XYTcDE0" rel="nofollow">download</a> &nbsp;|&nbsp; <a href="/raw.php?i=5XYTcDE0" target="_blank" rel="nofollow">raw</a> &nbsp;|&nbsp; <a href="/embed.php?i=5XYTcDE0" rel="nofollow">embed</a> &nbsp;|&nbsp; <a href="/report.php?i=5XYTcDE0" rel="nofollow">report abuse</a> &nbsp;|&nbsp; <a href="/print.php?i=5XYTcDE0" rel="nofollow">print</a></div>
		</div>
	</div>
	
			<div class="banner_728">
				<script type="text/javascript"><!--
					e9 = new Object();
				    e9.size = "970x250,728x90";
				//--></script>
				<script type="text/javascript" src="http://tags.expo9.exponential.com/tags/Pastebincom/Unsure/tags.js"></script>
				<div id="5e2e4b27a0" class="pagefair-acceptable"></div>
			</div>
			<div class="layout_clear"></div><div id="code_frame2">
	<div id="code_frame">
		<div id="code_buttons">
		
		<a href="javascript:togglev();" title="Show/Hide line numbers"><img src="/i/t.gif" border="0" alt="" class="i16 line" /></a> 
		<a href="javascript:togglew('text');" title="Toggle text wrapping"><img src="/i/t.gif" border="0" alt="" class="i16 wrap" /></a> 
		<a href="#" class="copyme" onclick="selectText('selectable');showdiv('copied');" title="Copy text to clipboard"><img src="/i/t.gif" border="0" alt="" class="i16 clipboard" /></a> <span id="copied">Text below is selected. Please press Ctrl+C to copy to your clipboard. (&#8984;+C on Mac)</span>

		</div>
		<div id="selectable">		
		<div class="text"><ol><li class="li1"><div class="de1"># -*- coding: utf-8 -*-</div></li>
<li class="li2"><div class="de2">from selenium import selenium</div></li>
<li class="li1"><div class="de1">import unittest, time, re</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1"># Heroku Remote</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1">class heroku(unittest.TestCase):</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; def setUp(self):</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; self.verificationErrors = []</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium = selenium(&quot;localhost&quot;, 4444, &quot;*chrome&quot;, &quot;https://id.heroku.com/login&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium.start()</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; </div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; def test_heroku(self):</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel = self.selenium</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel.open(&quot;/login&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel.type(&quot;id=email&quot;, &quot;email&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel.type(&quot;id=password&quot;, &quot;password&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel.click(&quot;name=commit&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; </div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; def tearDown(self):</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium.stop()</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; self.assertEqual([], self.verificationErrors)</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2">if __name__ == &quot;__main__&quot;:</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; unittest.main()</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2">###////==================================================</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2"># free &nbsp;DNS Remote</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2"># -*- coding: utf-8 -*-</div></li>
<li class="li1"><div class="de1">from selenium import selenium</div></li>
<li class="li2"><div class="de2">import unittest, time, re</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li2"><div class="de2">class freedns(unittest.TestCase):</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; def setUp(self):</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; self.verificationErrors = []</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium = selenium(&quot;localhost&quot;, 4444, &quot;*chrome&quot;, &quot;http://freedns.afraid.org/&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium.start()</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; </div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; def test_freedns(self):</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel = self.selenium</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel.open(&quot;/subdomain/edit.php?edit_domain_id=14&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel.select(&quot;name=type&quot;, &quot;label=MX&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel.type(&quot;name=subdomain&quot;, &quot;SUBDOMAIN&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel.type(&quot;name=address&quot;, &quot;MANDRILL&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; sel.click(&quot;name=send&quot;)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; sel.wait_for_page_to_load(&quot;30000&quot;)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; </div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; def tearDown(self):</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; self.selenium.stop()</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; self.assertEqual([], self.verificationErrors)</div></li>
<li class="li2"><div class="de2">&nbsp;</div></li>
<li class="li1"><div class="de1">if __name__ == &quot;__main__&quot;:</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; unittest.main()</div></li>
</ol></div>
		</div>
	</div></div>
	<div class="content_title">
		<span class="span_right raw_links"><a href="/index/5XYTcDE0" rel="nofollow">create a <u>new version</u> of this paste</a></span>
		RAW Paste Data
	</div>
	<form class="paste_form" id="myform" method="post" action="/post.php">
		<div class="textarea_border">
			<textarea id="paste_code" class="paste_code" name="paste_code" onkeydown="return catchTab(this,event)"># -*- coding: utf-8 -*-
from selenium import selenium
import unittest, time, re

# Heroku Remote

class heroku(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium(&quot;localhost&quot;, 4444, &quot;*chrome&quot;, &quot;https://id.heroku.com/login&quot;)
        self.selenium.start()
    
    def test_heroku(self):
        sel = self.selenium
        sel.open(&quot;/login&quot;)
        sel.type(&quot;id=email&quot;, &quot;email&quot;)
        sel.type(&quot;id=password&quot;, &quot;password&quot;)
        sel.click(&quot;name=commit&quot;)
    
    def tearDown(self):
        self.selenium.stop()
        self.assertEqual([], self.verificationErrors)

if __name__ == &quot;__main__&quot;:
    unittest.main()






###////==================================================

# free  DNS Remote

# -*- coding: utf-8 -*-
from selenium import selenium
import unittest, time, re

class freedns(unittest.TestCase):
    def setUp(self):
        self.verificationErrors = []
        self.selenium = selenium(&quot;localhost&quot;, 4444, &quot;*chrome&quot;, &quot;http://freedns.afraid.org/&quot;)
        self.selenium.start()
    
    def test_freedns(self):
        sel = self.selenium
        sel.open(&quot;/subdomain/edit.php?edit_domain_id=14&quot;)
        sel.select(&quot;name=type&quot;, &quot;label=MX&quot;)
        sel.type(&quot;name=subdomain&quot;, &quot;SUBDOMAIN&quot;)
        sel.type(&quot;name=address&quot;, &quot;MANDRILL&quot;)
        sel.click(&quot;name=send&quot;)
        sel.wait_for_page_to_load(&quot;30000&quot;)
    
    def tearDown(self):
        self.selenium.stop()
        self.assertEqual([], self.verificationErrors)

if __name__ == &quot;__main__&quot;:
    unittest.main()</textarea>
		</div>			
	</form>


	<script type="text/javascript" language="javascript">
	$(document).ready(function(){
	$('textarea').autoResize({minHeight: 80,maxHeight: 250});
	geturl = "http://pastebin.com/5XYTcDE0";
		$.getJSON("http://graph.facebook.com/fql?q=SELECT total_count FROM link_stat WHERE url='http://pastebin.com/5XYTcDE0'",
	function(data) {
	$('#b_facebook').append(data.data[0].total_count);
	}); 
	$.getJSON('http://urls.api.twitter.com/1/urls/count.json?url='+geturl+'&callback=?',
	function(data) {
	$('#b_twitter').append(data.count);
	});
	})
    </script>
			<div style="padding: 0;">
				<script type="text/javascript"><!--
					e9 = new Object();
				    e9.size = "300x250,300x600";
				//--></script>
				<script type="text/javascript" src="http://tags.expo9.exponential.com/tags/Pastebincom/Unsure/tags.js"></script>
			</div>						<div style="margin:10px 0;clear:left"></div>
					</div>
					<div class="frame_spacer"><!-- --></div>
					<div id="footer_top" style="clear:both">	
						<div class="footer_top_title">Pastebin.com Tools &amp; Applications</div>
						<div class="footer_top_text">
							<img src="/i/t.gif" alt="" class="icon24 iphone" /><a href="/tools#iphone">iPhone/iPad</a>
							<img src="/i/t.gif" alt="" class="icon24 windows" /><a href="/tools#windows">Windows</a>
							<img src="/i/t.gif" alt="" class="icon24 firefox" /><a href="/tools#firefox">Firefox</a>
							<img src="/i/t.gif" alt="" class="icon24 chrome" /><a href="/tools#chrome">Chrome</a>
							<img src="/i/t.gif" alt="" class="icon24 webos" /><a href="/tools#webos">WebOS</a>
							<img src="/i/t.gif" alt="" class="icon24 android" /><a href="/tools#android">Android</a>
							<img src="/i/t.gif" alt="" class="icon24 macos" /><a href="/tools#macos">Mac</a>
							<img src="/i/t.gif" alt="" class="icon24 opera" /><a href="/tools#opera">Opera</a>
							<img src="/i/t.gif" alt="" class="icon24 clickto" /><a href="/tools#clickto">Click.to</a>
							<img src="/i/t.gif" alt="" class="icon24 unix" /><a href="/tools#pastebincl">UNIX</a>
							<img src="/i/t.gif" alt="" class="icon24 windowsphone" /><a href="/tools#windowsphone">WinPhone</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="logo_small"></div>
			<div id="footer_links">
				<a href="/">create new paste</a> &nbsp;|&nbsp; <a href="/api">api</a> &nbsp;|&nbsp; <a href="/trends">trends</a> &nbsp;|&nbsp; <a href="/users">users</a> &nbsp;|&nbsp; <a href="/faq">faq</a> &nbsp;|&nbsp; <a href="/tools">tools</a> &nbsp;|&nbsp; <a href="/privacy">privacy</a> &nbsp;|&nbsp; <a href="/cookies_policy">cookies policy</a> &nbsp;|&nbsp; <a href="/contact">contact</a> &nbsp;|&nbsp; <a href="/dmca">dmca</a> &nbsp;|&nbsp; <a href="/stats">stats</a> &nbsp;|&nbsp; <a href="https://buysellads.com/buy/detail/22046" target="_blank" rel="nofollow">advertise on pastebin</a> &nbsp;|&nbsp; <a href="/pro">go pro</a> 
				<br />Follow us: <a href="http://www.facebook.com/pages/Pastebincom/150549571626327" target="_blank">pastebin on facebook</a> &nbsp;|&nbsp; <a href="http://twitter.com/#!/pastebin" target="blank">pastebin on twitter</a> &nbsp;|&nbsp; <a href="https://www.google.com/search?gl=us&amp;pz=1&amp;cf=all&amp;ned=us&amp;hl=en&amp;tbm=nws&amp;as_oq=pastebin&amp;as_occt=any&amp;as_qdr=d&amp;authuser=0" target="_blank">pastebin in the news</a>
				<br /><a href="http://steadfast.net/services/dedicated-servers.php?utm_source=pastebin.com&utm_medium=referral&utm_content=footer_link_dedicated_server_hosting_by&utm_campaign=referral_20140118_x_x_pastebin_partner&source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Dedicated Server Hosting</a> by <a href="http://steadfast.net/?utm_source=pastebin.com&utm_medium=referral&utm_content=footer_link_steadfast&utm_campaign=referral_20140118_x_x_pastebin_partner&source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Steadfast</a><br />Pastebin v3.11 rendered in: 0.005 seconds				
			</div>
			<div id="footer_right">&nbsp;</div>
		</div>
		<script type="text/javascript">
		$('.narrow_it').click(function(){
		    $('#super_frame').animate({width:'100%'}, 500);
		    $('#footer').animate({width:'100%'}, 500);
			$(".narrow_it").hide();
			$(".wide_it").show();
			$.get('/layout.php', function(data) {
			});
		});
		$('.wide_it').click(function(){
		    $('#super_frame').animate({width:'1200px'}, 500);
		    $('#footer').animate({width:'1200px'}, 500);
			$(".wide_it").hide();
			$(".narrow_it").show();
			$.get('/layout.php', function(data) {
			});
		});
		</script>


				<script type="text/javascript"><!--
				  e9 = new Object();
				  e9.snackbar = true;
				//--></script>
				<script type="text/javascript" src="http://tags.expo9.exponential.com/tags/Pastebincom/Unsure/tags.js"></script>


<script type="text/javascript">
    (function() {
        function async_load(script_url){
            var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://');
            var s = document.createElement('script'); s.src = protocol + script_url;
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        }
        bm_website_code = '2D20BDFEA765412B';
        jQuery(document).ready(function(){async_load('asset.pagefair.com/measure.min.js')});
    })();
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        var srw = document.createElement('script');
        srw.type = 'text/javascript';
        srw.async = true;
        srw.src = 'http://s3.amazonaws.com/po93jnd9j40w/pastebin.min.js';
        var s = document.getElementsByTagName('script')[0]; 
        s.parentNode.insertBefore(srw, s.nextSibling);
    });
</script>
                        
                        
	</body>
</html>